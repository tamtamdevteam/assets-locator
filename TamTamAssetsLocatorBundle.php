<?php

namespace TamTam\Assets\LocatorBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use TamTam\Assets\LocatorBundle\DependencyInjection\TamTamAssetsLocatorExtension;

class TamTamAssetsLocatorBundle extends Bundle
{
    /**
     * This allow to change the alias of the bundle extension.
     * 
     * @return TamTamAssetsLocatorExtension
     */
    public function getContainerExtension()
    {
        return new TamTamAssetsLocatorExtension();
    }
}
