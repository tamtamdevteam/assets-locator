<?php

namespace TamTam\Assets\LocatorBundle\Filter;

use Assetic\Asset\AssetInterface;
use Assetic\Filter\FilterInterface;
use Symfony\Component\HttpKernel\Kernel;

/**
 * Replace the urls in css
 */
class UrlFixer implements FilterInterface
{
    /** @var array The list of mapped urls */
    private $mapping = [];

    public function __construct($container)
    {
        if ($container->hasParameter('tamtam_url_fix')) {
            $this->mapping = $container->getParameter('tamtam_url_fix');
        }
    }

    public function filterDump(AssetInterface $asset)
    {
        if ($this->mapping) {
            $content = $asset->getContent();
            preg_match_all('/url\(([^)]*)\)/', $content, $matches);
            foreach (array_unique($matches[1]) as $path) {
                if (isset($this->mapping[$path])) {
                    $content = str_replace('url(' . $path . ')', 'url(' . $this->mapping[$path] . ')', $content);
                }
            }
            $asset->setContent($content);
        }
    }

    /**
     * Filters an asset after it has been loaded.
     *
     * @param AssetInterface $asset An asset
     */
    public function filterLoad(AssetInterface $asset)
    {
    }
}
